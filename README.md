# OpenML dataset: IEEE80211aa-GATS

https://www.openml.org/d/43254

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Data shows the downlink goodput for unicast and multicast transmissions with different group sizes and network loads on an IEEE 802.11ac network simulated on NS-3. Different IEEE 802.11aa GATS are used.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43254) of an [OpenML dataset](https://www.openml.org/d/43254). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43254/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43254/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43254/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

